package main

import (
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/esd-gpos/auth/middleware"
	"log"
	"net/http"
	"os"
)

func main() {
	// load env variables
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("could not load env")
	}

	router := gin.Default()

	info := &middleware.Information{
		Audience:  os.Getenv("AUD"),
		Issuer:    os.Getenv("ISSUER"),
		PublicKey: os.Getenv("PUBLIC_KEY"),
	}

	mw, err := middleware.NewMiddleware(info)
	if err != nil {
		log.Fatalf("erorr creating middleware: %s", err)
	}

	router.Use(mw.GinHandler)
	router.GET("ping", func(ctx *gin.Context) {
		userId := ctx.MustGet("userId").(string)
		ctx.JSON(http.StatusOK, gin.H{"user_id": userId})
	})

	log.Fatal(router.Run("0.0.0.0:9000"))
}
