package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
)

type Response struct {
	Message string `json:"message"`
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

func main() {
	// load env variables - from .env file at project root
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("could not load env")
	}

	// assign env variables
	quickstartAud := os.Getenv("QUICKSTART_AUD")
	issuer := os.Getenv("ISSUER")

	// setup middleware
	middleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (i interface{}, e error) {
			// verify audience claim
			checkAud := token.Claims.(jwt.MapClaims).VerifyAudience(quickstartAud, false)
			if !checkAud {
				return token, errors.New("invalid audience")
			}

			// verify issuer claim
			checkIss := token.Claims.(jwt.MapClaims).VerifyIssuer(issuer, false)
			if !checkIss {
				return token, errors.New("invalid issuer")
			}

			cert, err := getPermCert(token)
			if err != nil {
				panic(err.Error())
			}

			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
			return result, nil
		},
		SigningMethod: jwt.SigningMethodRS256,
	})

	// setup gin router
	router := gin.Default()

	// set public route
	router.GET("/api/public/ping", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"message": "I am always open to the public."})
	})

	// set private router
	private := router.Group("/api/private")

	// set authorization middleware
	private.Use(checkJwt(middleware))
	private.GET("/ping", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "You are authorized, you can view the private zone.",
			"claim":   ctx.MustGet("claims").(jwt.Claims),
		})
	})

	log.Fatal(router.Run("localhost:9000"))
}

// get permission certificate from link
func getPermCert(token *jwt.Token) (string, error) {
	// get our public key link
	publicKey := os.Getenv("PUBLIC_KEY")

	cert := ""
	resp, err := http.Get(publicKey)
	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)
	if err != nil {
		return cert, err
	}

	for k := range jwks.Keys {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("unable to find appropriate key")
		return cert, err
	}

	return cert, nil
}

// wrapper around actual middleware
func checkJwt(middleware *jwtmiddleware.JWTMiddleware) func(*gin.Context) {
	return func(ctx *gin.Context) {
		if err := middleware.CheckJWT(ctx.Writer, ctx.Request); err != nil {
			ctx.AbortWithStatus(http.StatusUnauthorized)
		}

		token, ok := ctx.Request.Context().Value("user").(*jwt.Token)
		if !ok {
			ctx.AbortWithStatus(http.StatusInternalServerError)
		}

		ctx.Set("claims", token.Claims)

		token = ctx.Request.Context().Value(middleware.Options.UserProperty).(*jwt.Token)
		claims := token.Claims.(jwt.MapClaims)
		fmt.Println(claims)
	}
}
