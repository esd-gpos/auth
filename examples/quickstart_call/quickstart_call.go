package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Request struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	Audience     string `json:"audience"`
	GrantType    string `json:"grant_type"`
}

type Response struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
}

func main() {
	// load env variables
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("could not load env")
	}

	// assign env variables
	url := os.Getenv("OAUTH")

	// create payload
	request := Request{
		ClientId:     os.Getenv("ID"),
		ClientSecret: os.Getenv("SECRET"),
		Audience:     os.Getenv("QUICKSTART_AUD"),
		GrantType:    "client_credentials",
	}

	payload, _ := json.Marshal(request)

	// create request
	req, _ := http.NewRequest("POST", url, bytes.NewReader(payload))
	req.Header.Add("content-type", "application/json")

	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()

	// unmarshal to struct
	var response Response
	body, _ := ioutil.ReadAll(res.Body)
	_ = json.Unmarshal(body, &response)

	// call api endpoint - from quickstart/quickstart.go
	req, _ = http.NewRequest(http.MethodGet, "http://localhost:9000/api/private/ping", nil)
	req.Header.Add("authorization", "Bearer "+response.AccessToken)

	res, _ = http.DefaultClient.Do(req)
	defer res.Body.Close()

	body, _ = ioutil.ReadAll(res.Body)
	fmt.Println(string(body))
}
