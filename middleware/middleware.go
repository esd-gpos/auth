package middleware

import (
	"encoding/json"
	"errors"
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
)

var (
	invalidAudience = errors.New("invalid audience")
	invalidIssuer   = errors.New("invalid issuer")
	invalidSubject  = errors.New("invalid subject")
	missingSubject  = errors.New("missing subject")
)

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

type Information struct {
	Audience  string
	Issuer    string
	PublicKey string
}

// Creates middleware with given information
// Handles updating jwks - caches it to reduce network calls
type Middleware struct {
	info          *Information
	jwks          *Jwks
	jwtMiddleware *jwtmiddleware.JWTMiddleware
}

func NewMiddleware(info *Information) (*Middleware, error) {
	middleware := &Middleware{info: info}
	if err := middleware.cacheJwks(); err != nil {
		return nil, err
	}

	middleware.setJWTMiddleware()
	return middleware, nil
}

// Fetches and populate jwks with given information
func (m *Middleware) cacheJwks() error {
	resp, err := http.Get(m.info.PublicKey)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&m.jwks)
	if err != nil {
		return err
	}

	return nil
}

// Creates and set our JWTMiddleware
func (m *Middleware) setJWTMiddleware() {
	m.jwtMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (i interface{}, e error) {
			// verify audience claim
			checkAud := token.Claims.(jwt.MapClaims).VerifyAudience(m.info.Audience, false)
			if !checkAud {
				return token, invalidAudience
			}

			// verify issuer claim
			checkIss := token.Claims.(jwt.MapClaims).VerifyIssuer(m.info.Issuer, false)
			if !checkIss {
				return token, invalidIssuer
			}

			cert, err := m.checkPermCert(token)
			if err != nil {
				panic(err.Error())
			}

			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
			return result, nil
		},
		SigningMethod: jwt.SigningMethodRS256,
	})
}

// Will check if token matches our certificate
func (m *Middleware) checkPermCert(token *jwt.Token) (string, error) {
	cert := ""
	for k := range m.jwks.Keys {
		if token.Header["kid"] == m.jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + m.jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("unable to find appropriate key")
		return cert, err
	}

	return cert, nil
}

// Handler middleware are below
// Our Gin package handler
func (m *Middleware) GinHandler(ctx *gin.Context) {
	if err := m.jwtMiddleware.CheckJWT(ctx.Writer, ctx.Request); err != nil {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	token := ctx.Request.Context().Value(m.jwtMiddleware.Options.UserProperty).(*jwt.Token)
	claims := token.Claims.(jwt.MapClaims)
	temp, ok := claims["sub"]
	if !ok {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	val, ok := temp.(string)
	if !ok {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	ctx.Set("userId", val)
}
